from cmd import Cmd
from git import Repo

class Profoflearn(Cmd):
    prompt = "PoL>"
    intro = "Hello the world ! Type ? to list commands"
    commande = ['-help', '-branch', '-whereami']
    #repo = Repo('D:\Python_trainning\\tasks')
    def __init__(self, gitpath):
        super(Profoflearn, self).__init__() ##need to call the constructor of the parent class (cmd.Cmd), There the completekey attribute is automatically declared with a default value
        self.repo = Repo(gitpath)

    def do_exit(self, inp):
        print("Bye")
        return True
    def help_exit(self):
        print(" Exit the Pol CLI")

    do_EOF = do_exit
    help_EOF = help_exit

    def default(self, inp):
        if inp == 'x' or inp == 'q':
            return self.do_exit(inp)

        print("Default usage : inv [argument]\n")
        print("If need help, type help")

    def do_inv(self, ri):
        '''Inv function is the entry of this program, you need type command like inv -[arg] to start specified process...'''
        if (ri == "-h") :
            self.do_help("inv")
        elif (ri == "-V") :
            print("Version ch1_0.1, not completed")
        elif (ri == "-l") :
            print("Availble task is below :")
            print("-branch -remote ----listing all of the remote branch")
            print("-branch -all ----listing all of the 'branch")
        elif (ri == "-branch remote") :
            result = self.repo.git.branch('-r')
            print(result)
            print("Number of branch : " + str(result.count("origin")))
        elif (ri == "-branch all") :
            result = self.repo.git.branch('-a')
            print(result)
            print("Number of branch : "+str(result.count("origin")))
        else :
            print("Availble command : ")
            print("-h  help")
            print("-V  version of program")
            print("-l  Availble task list")
            print("-branch remote  listing all of the remote branch")
            print("-branch all  listing all of the remote and local branch")

    def complete_veb(self, text, line, begidx, endidx):
        if not text:
            completions = self.commande[:]
        else :
            completions = [c for c in self.commande
                           if c.startswith(text)]
        return completions



if __name__ == '__main__':
    gitPath = ""
    while(gitPath == ""):
        gitPath = str(input('Please entry the path of repo you want to know -->'))
    Profoflearn(gitPath).cmdloop()

    #Profoflearn().cmdloop()
    print("End ch1 PoL program")